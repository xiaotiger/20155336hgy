	.file	"code.c"
	.section	.rodata
.LC0:
	.string	"not found"
.LC1:
	.string	"\346\226\207\344\273\266\345\205\263\351\227\255\345\244\261\350\264\245"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	addq	$8, %rax
	movq	(%rax), %rax
	movl	$0, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	open
	movl	%eax, -8(%rbp)
	cmpl	$-1, -8(%rbp)
	jne	.L2
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
.L2:
	movl	-8(%rbp), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	zhuanhuan
	movl	-8(%rbp), %eax
	movl	%eax, %edi
	call	close
	movl	%eax, -4(%rbp)
	cmpl	$-1, -4(%rbp)
	jne	.L4
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
.L4:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
.LC2:
	.string	"%x "
	.text
	.globl	zhuanhuan
	.type	zhuanhuan, @function
zhuanhuan:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	jmp	.L6
.L8:
	movzbl	-9(%rbp), %eax
	cmpb	$10, %al
	jne	.L7
	movl	$10, %edi
	call	putchar
	jmp	.L6
.L7:
	movzbl	-9(%rbp), %eax
	movsbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
.L6:
	leaq	-9(%rbp), %rcx
	movl	-20(%rbp), %eax
	movl	$1, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	read
	testq	%rax, %rax
	jne	.L8
	nop
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L9
	call	__stack_chk_fail
.L9:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	zhuanhuan, .-zhuanhuan
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
