Certificate Request:
    Data:
        Version: 1 (0x0)
        Subject: C=AU, O=Dodgy Brothers, CN=Dodgy CA
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:ea:c6:66:32:a6:a8:38:a4:29:53:f6:ac:fa:97:
                    aa:30:d1:81:3a:48:ed:f4:60:fa:62:9d:ad:fe:51:
                    a9:3b:82:fc:0f:6e:50:80:a1:54:01:ab:9b:ae:69:
                    97:1b:f4:79:c4:8e:22:46:3a:7b:9a:1c:56:fc:06:
                    4d:db:d9:c4:64:61:19:61:f6:2a:3e:ca:c2:00:8d:
                    30:0b:e7:ee:11:a5:a1:f1:1d:ab:4b:5c:dd:83:13:
                    4b:df:03:56:fe:fe:c2:51:fb:05:38:66:38:38:e7:
                    91:87:5c:40:95:4c:f4:a4:18:8e:97:d0:3c:f8:66:
                    71:65:2a:a3:cf:bc:b5:f1:04:8b:40:a9:6c:04:4a:
                    6b:69:fb:85:a3:3c:b9:da:43:62:18:ea:13:e0:2f:
                    81:e9:2b:0e:ce:94:f5:af:1d:eb:3b:60:0a:a3:62:
                    96:a9:7f:bd:da:13:97:e4:46:0a:21:ea:d9:d9:20:
                    4b:ae:79:6d:08:a6:b1:16:a2:93:65:64:2e:92:d2:
                    a3:77:6b:89:05:e5:9d:de:20:c6:30:db:b9:88:62:
                    76:c8:bf:9b:d5:b6:ba:7e:9f:d3:8b:31:3e:8c:2c:
                    9e:ed:69:b8:23:ca:c0:5b:6a:5a:ae:41:1a:7f:f8:
                    a8:3f:87:72:f9:53:4e:e7:3d:8e:b1:24:7c:d0:f2:
                    75:75
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha1WithRSAEncryption
         88:a5:f0:d6:91:1c:c9:0d:f3:7d:cb:75:7d:c7:03:85:d9:d0:
         68:65:e8:86:fc:6b:de:86:95:af:bd:bc:95:a7:fa:6f:cc:58:
         ce:77:d2:02:79:5d:e9:11:e9:0c:bf:11:76:58:55:86:2e:cf:
         ea:41:f9:87:a2:cb:10:64:df:55:2c:b8:a2:c3:5c:f2:80:d6:
         1b:70:44:9f:da:3a:34:2e:cc:c5:35:0c:8c:d3:b3:5a:ad:b6:
         fb:69:d5:4c:bd:5a:f7:68:1b:1d:0d:99:a0:63:09:f8:f4:2a:
         fc:1f:97:aa:88:32:c7:85:a9:7a:8e:86:ae:ab:2d:0a:ed:6b:
         98:96:3d:9f:81:83:b5:63:e1:74:bd:da:e2:62:7a:71:d2:5e:
         22:7b:ec:45:8d:29:bc:63:12:6c:39:0f:48:a2:29:f9:ff:4c:
         16:c9:eb:1a:31:05:c5:bd:25:22:21:a2:b0:0c:cf:3d:31:f1:
         69:cc:a9:87:5a:c9:ec:b9:1a:3a:35:30:e9:e9:df:7f:6b:90:
         9b:51:09:c1:3f:d0:8b:9c:9e:61:70:65:97:6a:2d:c7:fe:de:
         fe:00:3f:a3:e8:a1:cb:70:88:34:c6:20:cd:65:10:ae:0a:22:
         7a:c1:62:18:d6:9a:d2:3c:9d:4b:4b:5a:90:bd:66:c9:c3:3d:
         2f:c0:69:be
-----BEGIN CERTIFICATE REQUEST-----
MIICfjCCAWYCAQAwOTELMAkGA1UEBhMCQVUxFzAVBgNVBAoMDkRvZGd5IEJyb3Ro
ZXJzMREwDwYDVQQDDAhEb2RneSBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC
AQoCggEBAOrGZjKmqDikKVP2rPqXqjDRgTpI7fRg+mKdrf5RqTuC/A9uUIChVAGr
m65plxv0ecSOIkY6e5ocVvwGTdvZxGRhGWH2Kj7KwgCNMAvn7hGlofEdq0tc3YMT
S98DVv7+wlH7BThmODjnkYdcQJVM9KQYjpfQPPhmcWUqo8+8tfEEi0CpbARKa2n7
haM8udpDYhjqE+AvgekrDs6U9a8d6ztgCqNilql/vdoTl+RGCiHq2dkgS655bQim
sRaik2VkLpLSo3driQXlnd4gxjDbuYhidsi/m9W2un6f04sxPowsnu1puCPKwFtq
Wq5BGn/4qD+HcvlTTuc9jrEkfNDydXUCAwEAAaAAMA0GCSqGSIb3DQEBBQUAA4IB
AQCIpfDWkRzJDfN9y3V9xwOF2dBoZeiG/GvehpWvvbyVp/pvzFjOd9ICeV3pEekM
vxF2WFWGLs/qQfmHossQZN9VLLiiw1zygNYbcESf2jo0LszFNQyM07Narbb7adVM
vVr3aBsdDZmgYwn49Cr8H5eqiDLHhal6joauqy0K7WuYlj2fgYO1Y+F0vdriYnpx
0l4ie+xFjSm8YxJsOQ9Ioin5/0wWyesaMQXFvSUiIaKwDM89MfFpzKmHWsnsuRo6
NTDp6d9/a5CbUQnBP9CLnJ5hcGWXai3H/t7+AD+j6KHLcIg0xiDNZRCuCiJ6wWIY
1prSPJ1LS1qQvWbJwz0vwGm+
-----END CERTIFICATE REQUEST-----
